
% wireless networks ex1 Q4

%% part a
% The 2 - ray model Power vs distance graph

close all;
clear all;

%%%%%%%%%%%%%%%% you can change these params if you'd like %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R = -1; % according to goldsmith's plot in the book.
h_t = 50; % according to goldsmith's plot in the book.
h_r = 2; % according to goldsmith's plot in the book.
Gain_los = 1; % according to goldsmith's plot in the book.
Gain_reflected = 1; % according to goldsmith's plot in the book.
Pt = 1; % transmitting power
freq = [900*10^6,1850*10^6,2100*10^6];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

log_dist = 0:0.001:5; % according to goldsmith's plot in the book.
real_dist = 10.^log_dist; 

speed_of_light = 3*10^8;

length_reflected = sqrt((h_t + h_r)^2 + real_dist.^2); % reflected ray length
length_LOS = sqrt((h_t - h_r)^2 + real_dist.^2); % line of sight ray length
lambda = [0,0,0];

for i = 1:length(lambda)
lambda(i) = speed_of_light/freq(i); %lambda for 900mhz, 1850mhz, 2100 mhz.
end

los_factor = sqrt(Gain_los)./length_LOS; %direct ray factor in the equation

phase_shift = zeros(3,5001);
gr_factor = zeros(3,5001);
ps = zeros(3,5001);
Pr = zeros(3,5001);
p_normalized = zeros(3,5001);
norm = zeros(1,3);

for i = 1:length(lambda)
phase_shift(i,:) = 2*pi*(length_reflected-length_LOS)/lambda(i); % phase shift between the 2 rays
gr_factor(i,:) = R*sqrt(Gain_reflected)*exp(-1i.*phase_shift(i,:))./length_reflected;
ps(i,:) = abs((lambda(i)/4*pi).*( los_factor + gr_factor(i,:) )).^2;
%norm(i) = ps(i);
p_normalized(i,:) = ps(i,:)./ps(i); %normalize the power vector [currently in W]
Pr(i,:) = 10*log10(p_normalized(i,:)); %

figure
plot(log_dist , Pr(i,:));
grid on
title(['2 - ray model Power vs distance graph, freq = ', num2str(freq(i)/(10^6)) ,' [MHz]']);
xlabel('log_1_0(d)');
ylabel('Normalized received Power Pr [dB]');

end

%% part b - 10-ray model

close all;
clear all;

%%%%%%%%%%%%%%%% you can change these params if you'd like %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

R = -1; % according to goldsmith's plot in the book.
ant_height = 5;
t_wall_dist = 3; % according to goldsmith's plot in the book.
r_wall_dist = 1;
Gain_los = 1; % according to goldsmith's plot in the book.
Gain_reflected = 1; % according to goldsmith's plot in the book.
Pt = 1; % transmitting power, according to goldsmith's plot in the book.
freq = [900*10^6,1850*10^6,2100*10^6];

street_width = 10; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

log_dist = 0:0.001:5; % according to goldsmith's plot in the book.
ground_dist = 10.^log_dist;

speed_of_light = 3*10^8;

%length_reflected = sqrt((h_t + h_r)^2 + real_dist.^2); % reflected ray length

ref_ray_lengths(1,:) = sqrt((2*street_width - t_wall_dist - r_wall_dist)^2 + ground_dist.^2); % single wall reflected
ref_ray_lengths(2,:) = ref_ray_lengths(1,:); % single wall reflected
ref_ray_lengths(3,:) = sqrt((2*street_width - t_wall_dist + r_wall_dist)^2 + ground_dist.^2); % double wall reflected
ref_ray_lengths(4,:) = ref_ray_lengths(3,:); % double wall reflected
ref_ray_lengths(5,:) = sqrt((4*street_width - t_wall_dist - r_wall_dist)^2 + ground_dist.^2); % triple wall reflected
ref_ray_lengths(6,:) = ref_ray_lengths(5,:); % triple wall reflected
ref_ray_lengths(7,:) = sqrt((t_wall_dist + r_wall_dist)^2 + ground_dist.^2); % ground reflected

temp_for_WG_ray = sqrt(ground_dist.^2 + (t_wall_dist + r_wall_dist)^2);
x1 = sqrt((temp_for_WG_ray./2).^2 + ant_height^2);

ref_ray_lengths(8,:) = 2*x1; % WG ray length
ref_ray_lengths(9,:) = ref_ray_lengths(8,:); % GW ray length (same length as WG ray)

lambda = [0,0,0];
for i = 1:length(lambda)
lambda(i) = speed_of_light/freq(i); %lambda for 900mhz, 1850mhz, 2100 mhz.
end

length_LOS = sqrt((t_wall_dist - r_wall_dist)^2 + ground_dist.^2); % line of sight ray
los_factor = sqrt(Gain_los)./length_LOS; %direct ray factor in the equation

phase_shift = zeros(size(ref_ray_lengths,1),5001);
gr_factor = zeros(length(lambda),5001);
%ps = zeros(size(ref_ray_lengths,1),5001);
Pr = zeros(size(ref_ray_lengths,1),5001);
p_normalized = zeros(size(ref_ray_lengths,1),5001);
norm = zeros(1,size(ref_ray_lengths,1));


for j = 1:length(lambda)
    
for i = 1:size(ref_ray_lengths,1)
phase_shift(i,:) = 2*pi*(ref_ray_lengths(i,:)-ground_dist)/lambda(j); % phase shift between the 2 rays
gr_factor(j,:) = gr_factor(j,:) + R*sqrt(Gain_reflected)*exp(-1i.*phase_shift(i,:))./length_LOS;
end

ps(j,:) = abs((lambda(j)/4*pi).*(los_factor + gr_factor(j,:))).^2;
%norm(i) = ps(i);
p_normalized(j,:) = ps(j,:)./ps(j); %normalize the power vector [currently in W]
Pr(j,:) = 10*log10(p_normalized(j,:)); %

figure
plot(log_dist , Pr(j,:));
grid on
title(['10 - ray model Power vs distance graph, freq = ', num2str(freq(j)/(10^6)) ,' [MHz]']);
xlabel('log_1_0(d)');
ylabel('Normalized received Power Pr [dB]');

end
