clear all;
close all;
clc;

% down wall
x1 = 0; 
y1 = 1;
x2 = 20;
y2 = 1;

%point where the transmitter is at
x3 = 10;
y3 = 0;

v1 = 20;
v2 = 10;

P1 = [x1,y1];
P2 = [x2,y2];
P3 = [x3,y3];

v = [v1,v2];
%line(v,P3); %(10,0) - (10,20)

t = ((y2-y3)*v1-(x2-x3)*v2)/((x1-x2)*v2-(y1-y2)*v1);
P4 = t*P1+(1-t)*P2;
P5 = P3+2*dot(P4-P3,P2-P1)/dot(P2-P1,P2-P1)*(P2-P1);


%hold on;
%line(P2,P1)


%plot(P4(1),P4(2), 'ro', 'Markersize', 5)
%plot(P5(1),P5(2), 'ro', 'Markersize', 5)

%plot(x_intersect,y_intersect, 'ro', 'Markersize', 5)

%hold off;
A = [10 15]; 
B = [0 7]; 
plot(A,B,'*')

hold on;
line(A,B)

xlim = get(gca,'XLim');
ylim = get(gca,'YLim');
m = (B(2)-B(1))/(A(2)-A(1));
n = B(2)*m - A(2);
y1 = m*xlim(1) + n;
y2 = m*xlim(2) + n;

line([xlim(1) xlim(2)],[y1 y2])

daspect([1 1 1]);
rectangle('Position',[0 0 20 30]) % room
rectangle('Position',[4 10 12 2],'FaceColor','k') % big barrier
rectangle('Position',[9.9 0 0.2 0.2],'FaceColor','c') % transmitter
rectangle('Position',[9.5 15 1 0.1],'FaceColor','g') % receiver

hold off


%{
% upper wall
x1 = 1; 
y1 = 30;
x2 = 5;
y2 = 30;

% left side wall
x1 = 0; 
y1 = 20;
x2 = 0;
y2 = 10;

% right side wall
x1 = 20; 
y1 = 20;
x2 = 20;
y2 = 5;

%}